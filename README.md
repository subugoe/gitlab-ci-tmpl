# GitLab CICD template

This repo (currently) contains a `.gitlab-ci.yml` template which performs three jobs, given a main repo (with the application) and a helm repo:

1. Optional: Increase the application version in the main repo.
2. Create a docker image for the application.
3. Update the application version and the image tag in the helm repo, and increase the helm version in the helm repo.

In detail, these steps do the following:

If application versioning (via `npm`) is enabled, a push on the `main` branch in the main repo triggers the **first job** that increases `"version"` in the `package.json` file. The change is then pushed to main with the commit message `"Change version to ..."`, where `...` is the new version number. This push then triggers the other two jobs. If application versioning is disabled, a push on the `main` branch directly triggers the second and the third job.

The **second job** builds an image of the application.

The **third job** updates the image tag in the helm repo's `values.yaml` file, and increases `version` in the `Chart.yaml` file. If application versioning is enabled, it also sets the `appVersion` to the version of the main repo. The changes are then pushed to main (in the helm repo) with the same commit message as in the first job. If application versioning is disabled, the commit message is simply `"Update version"`.

## Requirements

### Repos

The template assumes that you have two repos within the same (sub)group:

- main repo: https://gitlab.gwdg.de/group/subgroup/application
- helm repo: https://gitlab.gwdg.de/group/subgroup/application-helm

(`group`, `subgroup` and `application` are placeholders)

### Access Token

You have to create an access token with read/write permission on the level of `subgroup`. Copy the value of the token and store it in a CI variable (also on the level of `subgroup`). Per default, the template assumes that the variable has the name `CI_ACCESS_TOKEN`.

### Helm Repo

The helm repo should have the following structure:

```
application-helm
└── application
    ├── templates
    ├── Chart.yaml
    └── values.yaml
```

(This is the structure created by running `helm create application`).

### Main Repo

In the main repo, add a file `.gitlab-ci.yml` with the following content:

```yml
include:
  - project: 'subugoe/gitlab-ci-tmpl'
    ref: 'main'
    file: '.gitlab-ci.yml'
```

That's basically it. You might want or have to specify some additional parameters, which you can do via the `inputs` field:

```yml
include:
  - project: 'subugoe/gitlab-ci-tmpl'
    ref: 'main'
    file: '.gitlab-ci.yml'
    inputs:
        parameter_1: value_1
        parameter_2: value_2
        # ...
```

The currently supported parameters are described in the following.

## Parameters

- `access_token` (default: `${CI_ACCESS_TOKEN}`)  
The access token to use for `git` commits.
- `app_versioning` (default: `""`)  
Only if this parameter is not `""`, the application version in the main repo is increased and updated in the helm repo. The only currently supported non-empty value is `"npm"`, which indicates versioning via `npm` and requires the file `package.json` in the main repo.
- `branches_dev` (default: `"^(develop|main|master)$"`)  
A regular expression matching the branches of the project's development version.
- `branches_main` (default: `"^(main|master)$"`)  
A regular expression matching the branches of the project's main/production version.
- `branches_staging` (default: `"^staging$"`)  
A regular expression matching the branches of the project's staging version.
- `branches_test` (default: `".*"`)  
A regular expression matching the branches of the project's test version.
- `pipeline_source_events` (default: `"push"`)  
A regular expression matching the possible values of `$CI_PIPELINE_SOURCE`.
- `values_file_dev` (default: `""`)  
Name of (or path to) the values yaml file in the helm repo corresponding to the project's development version.
- `values_file_main` (default: `"values.yaml"`)  
Name of (or path to) the values yaml file in the helm repo corresponding to the project's main/production version.
- `values_file_staging` (default: `""`)  
Name of (or path to) the values yaml file in the helm repo corresponding to the project's staging version.
- `values_file_test` (default: `""`)  
Name of (or path to) the values yaml file in the helm repo corresponding to the project's test version.

## Projects

The following projects use this template:

- [ahiqar] tokenize-me-api  
https://gitlab.gwdg.de/subugoe/ahiqar/tokenize-me-api  
https://gitlab.gwdg.de/subugoe/ahiqar/tokenize-me-api-helm
- [EUPT] eupt-textapi  
https://gitlab.gwdg.de/subugoe/eupt/eupt-textapi  
https://gitlab.gwdg.de/subugoe/eupt/eupt-textapi-helm
- [EUPT] Website  
https://gitlab.gwdg.de/subugoe/eupt/website  
https://gitlab.gwdg.de/subugoe/eupt/website-helm
- TextGrid Python sid API  
https://gitlab.gwdg.de/textgrid-python-sid/textgrid-python-sid-api  
https://gitlab.gwdg.de/textgrid-python-sid/textgrid-python-sid-api-helm
